/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm2;

/**
 *
 * @author 66955
 */
public class Human {
     protected String name;
     protected int age;
     protected String gender;
     protected String food ;
     protected String snack ;
     
     
     public Human(String name,int age,String gender) {
         this.name = name;
         this.age = age;
         this.gender = gender;
    }
     
     public void speak(){
         System.out.println("Human Speak !!!");
         System.out.println("My name is : "+this.name);
         System.out.println("I am "+this.gender);
         System.out.println("I am "+this.age+" years old");
     }
     
     public void eat(){
         System.out.println("I like to eat snack in the morning!!");
     }
     
     public void eat(String food){
         System.out.println("I like to eat food in the morning!!");
     }
      
     public void eat(String food,String snack){
         System.out.println("I like to eat food&snack in the morning!!");
     }



     
}
